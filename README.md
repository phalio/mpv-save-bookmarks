# mpvs

A little script for mpv that allows saving and loading named bookmarks and remembering the last watched video.

## Usage

`mpvs <link|p> <name>`

No arguments: Play last saved video  
First argument `p`, second argument `name`: Play video saved as "name"  
First argument `link/to/video`, no second argument: Save as last video and play  
First argument `link/to/video`, second argument `name`: Save video as "name" and play

Playing any video with `mpvs` always saves it as last, and only additionally under a name if a name is specified. Names can be reused to override what is currently saved as that name.

### Examples

The most common use case is for watching longer videos or series that you won't watch all at once. Therefore it's best combined with `--save-position-on-quit` to also save the position you were in. This flag is used by default when using `mpvs`.

To play a video and then later continue watching this last watched video or watch it again:

```shell
mpvs https://youtu.be/jAXioRNYy4s    # play the video and save it as last video
mpvs                                 # play the last watched video
```

To play a video and save it as a named bookmark:

```shell
mpvs https://youtu.be/0s20588opz4 angel    # play the video and save it as "angel"
mpvs p angel                               # play the video bookmarked as "angel"
```

To watch an episode of a series of long videos and always save the episode currently being watched under the same name (or, for example, watch Twitch VODs in general and override the bookmark for a channel with the next more recent VOD once done):

```shell
mpvs https://www.twitch.tv/videos/1296574805 sheila    # play Neon Divide Episode 1 (Sheila's POV) and save it as "sheila"
mpvs p sheila                                          # continue watching episode 1 which is bookmarked as "sheila"
mpvs https://www.twitch.tv/videos/1303206377 sheila    # play episode 2 and override the bookmark "sheila" with episode 2
mpvs p sheila                                          # continue watching the bookmark "sheila", which is now episode 2
```

Using file synchronisation tools like [Syncthing](https://syncthing.net/) (the default bookmark directory is `~/.config/mpv/bookmarks/`) allows sharing the same bookmarks between devices, possibly combined with sharing the saved positions in `~/.config/mpv/watch_later/`.
	
### Configuration

In the script's first section, you can change the path mpvs uses to save bookmarks and add mpv arguments that you only want to apply when using mpvs.

## TODO

- option to only save and not play
- list saved bookmarks
- delete bookmarks
- save last 10/any videos instead of only the last
- use a single bookmark file (though mpv also uses separate files per saved position)
- allow for arguments to be passed through to mpv

## Irrelevant Backstory

This is just a script I quickly put together for myself to be able to save bookmarks for mpv, as outlined in the usage examples above. It's not intended to be something fancy and well thought out, I just put it here because I thought others might find it useful. The first version I put up here was made in one afternoon on the PinePhone + Keyboard Case. I initially named it mpvb for mpv-bookmarks, but I found mpvs (s for save)  easier to type and prettier. I would have preferred to name the repository "mpv-bookmarks", but I decided to name it "mpv-save-bookmarks" since having the S of the actual command there probably makes it clearer what the S stands for and makes the command name easier to remember. "bookmarks" of course also has to be included in the name to make it clear what it saves. The name is a little long now unfortunately, but it's probably the best mix of descriptiveness and relatedness to the script's name.
